package com.enums;

public enum Currency {
    RU(1),
    USD(75),
    EUR(85);

    private final int price;

    private Currency(int p) {
        price = p;
    }

    public int getPrice(){
        return price;
    };

}
