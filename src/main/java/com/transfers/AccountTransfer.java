package com.transfers;

import com.clients.Recipient;
import com.clients.Sender;
import com.enums.Currency;

public class AccountTransfer extends Transfer{

    public AccountTransfer(int amount, Currency currency, Recipient recipient, Sender sender) {
        super(amount, currency, recipient, sender);
    }

    @Override
    public String toString(){
        return transferToString(
                "ПЕРЕВОД СО СЧЕТА НА СЧЕТ: ",
                ", Номер карты получателя: " + getRecipient().getAccountNumber(),
                ", Номер карты отправителя: " + getSender().getAccountNumber()
        );
    };
}

