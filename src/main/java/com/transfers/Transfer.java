package com.transfers;
import com.clients.Recipient;
import com.clients.Sender;
import com.enums.Currency;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class Transfer {
    private final int number;
    private int amount;
    private Currency currency;
    private Recipient recipient;
    private Sender sender;
    private final LocalDateTime date;

    public Transfer(int amount, Currency currency, Recipient recipient, Sender sender) {
        this.number = (int) (Math.random() * 10000);
        this.date = LocalDateTime.now();
        this.amount = amount;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        try {
            checkAmount(amount, sender, currency);
            sender.setBalance(sender.getBalance() - amount);
            recipient.setBalance(recipient.getBalance() + amount);

        } catch (IllegalArgumentException ignored) {
            System.out.println("Ошибка:перевод № " + number +" не исполнен. Недостаточно средств");
        }
    }

    public String transferToString(String transferName, String recipientAccountData, String senderAccountData){
            return  transferName + "Номер перевода: " + number +
                    ", Дата: " + getEuropeanDateAndTime() +
                    recipientAccountData +
                    senderAccountData +
                    ", Валюта: " + currency +
                    ", Сумма: " + amount +
                    ", Получатель: " + recipient +
                    ", Отправитель: " + sender;

        };

    public String getEuropeanDateAndTime(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return formatter.format(date);
    }

    public void checkAmount (int amount, Sender sender, Currency currency) {
        if (amount * currency.getPrice() > sender.getBalance()){
            throw new IllegalArgumentException();
        }
    };

    @Override
    public boolean equals (Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Transfer transfer = (Transfer) object;
        if (this.hashCode() != transfer.hashCode()) {
            return false;
        }
        return this.getNumber() == transfer.getNumber() &&
                this.getAmount() == transfer.getAmount() &&
                this.getCurrency() == transfer.getCurrency() &&
                this.recipient.equals(transfer.recipient) &&
                this.sender.equals(transfer.sender) &&
                this.date.equals(transfer.date);

    };

    @Override
    public int hashCode() {
        return  31 * (number +
                31 * (amount) +
                31 * (currency.getPrice() +
                31 * (recipient.hashCode() +
                31 * (sender.hashCode() +
                31 * (date.hashCode())))));
    }
}
