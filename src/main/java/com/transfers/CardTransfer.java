package com.transfers;

import com.clients.Recipient;
import com.clients.Sender;
import com.enums.Currency;

public class CardTransfer extends Transfer{

    public CardTransfer(int amount, Currency currency, Recipient recipient, Sender sender) {
        super(amount, currency, recipient, sender);

    }

    @Override
    public String toString(){

        return transferToString(
                "ПЕРЕВОД С КАРТЫ НА КАРТУ: ",
                ", Номер карты получателя: " + getRecipient().getCardNumber(),
                ", Номер карты отправителя: " + getSender().getCardNumber()
        );
    };


}
