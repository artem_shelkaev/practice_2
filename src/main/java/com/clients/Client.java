package com.clients;

import lombok.Data;
import java.util.Locale;

@Data
public class Client {
    private String surname;
    private String name;
    private String patronymic;
    private int accountNumber;
    private int cardNumber;
    private int balance;

    public Client(String surname, String name, String patronymic, int accountNumber, int cardNumber, int balance) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.accountNumber = accountNumber;
        this.cardNumber = cardNumber;
        this.balance = balance;
    }

    public String getFullNameToUpperCase(){
        return toString().toUpperCase(Locale.ROOT);
    }

    public String getFullNameToLowerCase(){
        return toString().toLowerCase(Locale.ROOT);
    }

    @Override
    public String toString(){
        return surname + " " + name + " " + patronymic;
    }

    @Override
    public boolean equals (Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Client client = (Client) object;

        if (this.hashCode() != client.hashCode()) {
            return false;
        }
        return this.getSurname() == client.getSurname() &&
                this.getName() == client.getName() &&
                this.getPatronymic() == client.getPatronymic() &&
                this.getAccountNumber() == client.getAccountNumber() &&
                this.getCardNumber() == client.getCardNumber() &&
                this.getBalance() == client.getBalance();

    };

    @Override
    public int hashCode() {
        return  31 * (surname.hashCode() +
                31 * (name.hashCode() +
                31 * (patronymic.hashCode() +
                31 * (accountNumber +
                31 * (cardNumber +
                31 * (balance))))));
    }

}
