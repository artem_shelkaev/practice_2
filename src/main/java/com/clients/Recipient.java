package com.clients;

public class Recipient extends Client{

    public Recipient(String name, String surname, String patronymic,
                     int accountNumber, int cardNumber, int balance
    ){
        super(name, surname, patronymic, accountNumber, cardNumber, balance);
    }
}
