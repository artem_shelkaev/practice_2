package com.clients;

public class Sender extends Client{

    public Sender(String name, String surname, String patronymic,
                  int accountNumber, int cardNumber, int balance
    ){
        super(name, surname, patronymic, accountNumber, cardNumber, balance);
    }
}
