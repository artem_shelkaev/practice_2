package com;

import com.clients.Recipient;
import com.clients.Sender;
import com.enums.Currency;
import com.transfers.AccountTransfer;
import com.transfers.CardTransfer;


public class Main {
    public static void main(String[] args) {

        Sender sender = new Sender("Отправлятов", "Отправлят", "Отправлятович",
                4455, 6677, 45000);
        Recipient recipient = new Recipient("Получатов", "Получат", "Получатович",
                1122, 3344, 10000);

        // Клиент
        System.out.println("-------------------------------------------------------");
        System.out.println(sender);
        System.out.println(sender.getFullNameToUpperCase());
        System.out.println(sender.getFullNameToLowerCase());
        System.out.println(recipient);
        System.out.println(recipient.getFullNameToUpperCase());
        System.out.println(recipient.getFullNameToLowerCase());

        // Перевод
        System.out.println("-------------------------------------------------------");
        CardTransfer cardTransfer = new CardTransfer(1000, Currency.RU, recipient, sender);
        AccountTransfer accountTransfer_1 = new AccountTransfer(1, Currency.EUR, recipient, sender);
        AccountTransfer accountTransfer_2 = new AccountTransfer(10, Currency.USD, recipient, sender);

        System.out.println(cardTransfer);
        System.out.println(accountTransfer_1);
        System.out.println(accountTransfer_2);

        // Ошибка перевода
        System.out.println("-------------------------------------------------------");
        CardTransfer bedCardTransfer = new CardTransfer(45000, Currency.RU, recipient, sender);
        AccountTransfer bedAccountTransfer_1 = new AccountTransfer(1000, Currency.EUR, recipient, sender);
        AccountTransfer bedAccountTransfer_2 = new AccountTransfer(1000, Currency.USD, recipient, sender);

        // Сравнение +
        System.out.println("-------------------------------------------------------");
        Sender sender_1 = new Sender("Отправлятов", "Отправлят", "Отправлятович",
                4455, 6677, 45000);
        Recipient recipient_1 = new Recipient("Получатов", "Получат", "Получатович",
                1122, 3344, 10000);
        Sender same_sender_1 = new Sender("Отправлятов", "Отправлят", "Отправлятович",
                4455, 6677, 45000);
        Recipient same_recipient_1 = new Recipient("Получатов", "Получат", "Получатович",
                1122, 3344, 10000);
        
        System.out.println(sender_1.equals(same_sender_1));
        System.out.println(recipient_1.equals(same_recipient_1));
        System.out.println(cardTransfer.equals(cardTransfer));
        System.out.println(accountTransfer_1.equals(accountTransfer_1));

        // Сравнение -
        System.out.println("-------------------------------------------------------");
        System.out.println(sender_1.equals(sender));
        System.out.println(recipient_1.equals(recipient));
        System.out.println(cardTransfer.equals(bedCardTransfer));
        System.out.println(accountTransfer_1.equals(bedAccountTransfer_1));

    }
}
